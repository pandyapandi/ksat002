<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>S1 Book appointment</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>3a96facb-1793-4641-813e-a4b5596878f0</testSuiteGuid>
   <testCaseLink>
      <guid>e732907a-1b55-4637-baaf-96c7704f955c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Blocks/02 Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>8a3e9d03-6ac4-487b-b5fa-1c1228b8f987</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/account_list</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>8a3e9d03-6ac4-487b-b5fa-1c1228b8f987</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>4b11a8dd-28cd-463d-9346-2e16c008cce1</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>8a3e9d03-6ac4-487b-b5fa-1c1228b8f987</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>de6c85bf-0d29-47a8-afd5-0138e14ea2c1</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
